Django==3.2
psycopg2-binary==2.9.1

djangorestframework==3.12.4


Pillow==8.3.1
django_heroku
gunicorn 
whitenoise 