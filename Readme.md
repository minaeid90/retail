**Running project on local**

- clone the project
  > `git clone https://minaeid90@bitbucket.org/minaeid90/retail.git`
- make virtualenv
  > `virtualenv venv`
- activate venv
  >`source ./venv/bin/activate`
- install packages used in the project
  > `pip install –r requirements.txt`
- migrate migration to the database
  > `python manage.py migrate`
- create super user
  > `python manage.py createsuperuser`
- run the server
  > `python manage.py runserver`
