from django.contrib import admin

from .models import Store, ProductCategory,  Product, StockReading

admin.site.register(Store)
admin.site.register(Product)
admin.site.register(ProductCategory)
admin.site.register(StockReading)
