
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.db.models import fields
from django.utils import timezone
from django.contrib.auth import get_user_model
# from gtin_fields.fields import GTIN14Field
from django.db.models.signals import post_delete, post_save
from django.dispatch import receiver

User = get_user_model()


class TimeStamp(models.Model):
    """Model definition for TimeStamp."""

    crrated_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        """Meta definition for TimeStamp."""
        abstract = True


class StockReadingManager(models.Manager):
    def updated_stocks(self, last_pulled_at):
        return super().get_queryset().filter(type='update', timestamp__gte=last_pulled_at)

    def created_stocks(self, last_pulled_at):
        return super().get_queryset().filter(type='create', timestamp__gte=last_pulled_at)

    def deleted_stocks(self, last_pulled_at):
        return super().get_queryset().filter(type='delete', timestamp__gte=last_pulled_at)


class StockReading(TimeStamp):
    """Model definition for Stock Reading."""
    ref = models.BigIntegerField()
    change = models.JSONField(null=True, blank=True)
    TYPES = (
        ('create', 'Create'),
        ('update', 'Update'),
        ('delete', 'Delete'),
    )
    type = models.CharField(
        max_length=30, choices=TYPES, null=True, blank=True)
    timestamp = models.DateTimeField(null=True, blank=True)
    objects = models.Manager()
    changes = StockReadingManager()

    class Meta:
        """Meta definition for Stock Reading."""

        verbose_name = 'Stock Reading'
        verbose_name_plural = 'Stock Readings'

    def __str__(self):
        """Unicode representation of Stock Reading."""
        return f'{self.id}'


class Store(TimeStamp):
    """Model definition for Store."""

    name = models.CharField(max_length=50)
    employee = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True, blank=True)

    class Meta:
        """Meta definition for Store."""

        verbose_name = 'Store'
        verbose_name_plural = 'store'

    def __str__(self):
        """Unicode representation of Store."""
        return self.name


class ProductCategory(TimeStamp):
    """Model definition for Product Category."""

    name = models.CharField(max_length=50, unique=True)

    class Meta:
        """Meta definition for ProductCategory."""

        verbose_name = 'Product Category'
        verbose_name_plural = 'Product Categories'

    def __str__(self):
        """Unicode representation of Product Category."""
        return self.name


class Product(TimeStamp):
    """Model definition for Product."""

    name = models.CharField(max_length=50)
    category = models.ForeignKey(ProductCategory, on_delete=models.CASCADE)
    store = models.ForeignKey(Store, on_delete=models.CASCADE)
    barcode = models.PositiveIntegerField(unique=True, default=0)
    expire_date = models.DateField(default=timezone.now)
    quantity = models.PositiveBigIntegerField(default=0)

    @property
    def in_stock(self):
        return True if self.quantity > 0 else False

    class Meta:
        """Meta definition for Product."""

        verbose_name = 'Product'
        verbose_name_plural = 'Products'

    def __str__(self):
        """Unicode representation of Product."""
        return self.name


@receiver(post_save, sender=Product)
def product_created_updated(sender, instance, created, **kwargs):
    data = {
        'id': instance.id,
        'name': instance.name,
        'category': instance.category.id,
        'store': instance.store.id,
        'barcode': instance.barcode,
        'expire_date': instance.expire_date.strftime('%Y-%m-%d'),
        'quantity': instance.quantity,
        'in_stock': instance.in_stock,
    }
    if created:
        StockReading.objects.create(
            ref=instance.id,
            change=data,
            type='create',
            timestamp=timezone.now()
        )
    else:
        try:
            ref = StockReading.objects.filter(pk=instance.id).update(change=data)
        except ObjectDoesNotExist:
            StockReading.objects.create(
                ref=instance.id,
                change=data,
                type='update',
                timestamp=timezone.now()

            )


@receiver(post_delete, sender=Product)
def prodcut_deleted(sender, instance, **kwargs):
    StockReading.objects.create(
        ref=instance.id,
        change=instance.id,
        type='delete',
        timestamp=timezone.now(),
    )
