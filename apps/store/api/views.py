import json
from datetime import datetime

from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from ..models import Product, ProductCategory, StockReading, Store
from .serializers import StockReadingSerializer


@api_view(['GET', 'POST'])
def stock_reading(request):
    if request.method == 'GET':
        query = request.GET.get('last_pulled_at')
        if query == '0':
            stock_readings = StockReading.objects.all()
            serializer = StockReadingSerializer(stock_readings, many=True)
            return Response(serializer.data)

        elif query:
            try:
                timestamp = timezone.datetime.strptime(
                    query, '%Y-%m-%d %H:%M:%S')
                created_obj = StockReading.changes.created_stocks(timestamp)
                updated_obj = StockReading.changes.updated_stocks(timestamp)
                deleted_obj = StockReading.changes.deleted_stocks(timestamp)

                return Response({
                    'changes': {
                        'products': {
                            'created': [obj.change for obj in created_obj],
                            'updated': [obj.change for obj in updated_obj],
                            'deleted': [obj.change for obj in deleted_obj]
                        }
                    },
                    'timestamp': timezone.now(),
                })

            except Exception as e:
                return Response({'details': str(e)}, status=status.HTTP_404_NOT_FOUND)
        return Response({}, status=status.HTTP_204_NO_CONTENT)

    elif request.method == 'POST':

        data = json.loads(request.body)
        stocks = data['changes']['products']
        created = stocks['created']
        updated = stocks['updated']
        deleted = stocks['deleted']
        timestamp = datetime.strptime(
            data['timestamp'], '%Y-%m-%d %H:%M:%S')
        timestamp = timezone.make_aware(
            timestamp, timezone=timezone.get_default_timezone())

        created_objs = []
        for c in created:
            product = Product(
                name=c['name'],
                category=ProductCategory.objects.get(pk=c['category']),
                store=Store.objects.get(pk=c['store']),
                barcode=c['barcode'],
                expire_date=timezone.datetime.strptime(
                    c['expire_date'], '%Y-%m-%d'),
                quantity=c['quantity'],
            )
            created_objs.append(product)
        Product.objects.bulk_create(created_objs)

        updated_objs = []
        for c in updated:
            try:
                updated_at = Product.objects.get(pk=c['id']).updated_at
                if timestamp > updated_at:
                    product = Product(
                        id=c['id'],
                        name=c['name'],
                        category=ProductCategory.objects.get(pk=c['category']),
                        store=Store.objects.get(pk=c['store']),
                        barcode=c['barcode'],
                        expire_date=timezone.datetime.strptime(
                            c['expire_date'], '%Y-%m-%d'),
                        quantity=c['quantity'],
                    )
                    updated_objs.append(product)
            except ObjectDoesNotExist:
                continue

        Product.objects.bulk_update(
            updated_objs, fields=['name', 'category', 'store', 'barcode', 'expire_date', ])

        deleted_objs = deleted
        deleted_objs = []
        for c in deleted_objs:
            try:
                Product(id=c).delete()
            except:
                continue
    return Response(status.HTTP_200_OK)
