import json
from datetime import datetime
from django.utils import timezone
from rest_framework import fields, serializers, status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from ..models import StockReading


class StockReadingSerializer(serializers.ModelSerializer):
    class Meta:
        model = StockReading
        fields = '__all__'
