from rest_framework import routers
from django.urls import path

from .views import stock_reading


urlpatterns = [
    path('sync/', stock_reading),
]
